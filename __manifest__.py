{
    'name': 'Point of Sale Logo',
    'version': '10.1',
    'summary': """Logo For Every Point of Sale (Screen & Receipt)""",
    'description': """"This module helps you to set a logo for every point of sale. This will help you to
                 identify the point of sale easily. You can also see this logo in pos screen and pos receipt.""",
    'category': 'Point Of Sale',
    'author': 'ehAPI',
    'company': 'ehAPI',
    'website': "http://www.ehapi.com",
    'depends': ['base', 'crm','point_of_sale'],
    'qweb': ['static/src/xml/pos_ticket_view.xml',
             'static/src/xml/pos_screen_image_view.xml',
             ],
            
    'data': [
        'views/template.xml',
        'views/pos_config_image_view.xml',
        'views/pos_image_view.xml',
        'views/custom_crm_settings.xml',
    ],
   
    'images': ['static/description/pos.png'],
    # 'license': 'AGPL-3',
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
